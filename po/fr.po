# French translation for ubuntu-ui-extras
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd.
# This file is distributed under the same license as the ubuntu-ui-extras package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-extras\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-19 16:19+0000\n"
"PO-Revision-Date: 2023-01-15 17:49+0000\n"
"Last-Translator: Timothy G. <22472919+GTimothy@users.noreply.github.com>\n"
"Language-Team: French <https://hosted.weblate.org/projects/lomiri/lomiri-ui-"
"extras/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2017-03-19 06:38+0000\n"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:106
msgid "Aborted"
msgstr "Interrompu"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:108
msgid "Active"
msgstr "Actif"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:215
#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:72
msgid "Cancel"
msgstr "Annuler"

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:44
msgid "Color"
msgstr "Couleur"

#: modules/Lomiri/Components/Extras/Printers/models/printermodel.cpp:54
msgid "Create PDF"
msgstr "Créer un fichier PDF"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:43
#: modules/Lomiri/Components/Extras/PhotoEditor/CropOverlay.qml:349
msgid "Crop"
msgstr "Recadrer"

#: modules/Lomiri/Components/Extras/PhotoEditor/ExposureAdjuster.qml:80
msgid "Done"
msgstr "Terminé"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:235
msgid "Enhancing photo..."
msgstr "Amélioration de la photo..."

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:61
msgid "Exposure"
msgstr "Exposition"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:104
msgid "Idle"
msgstr "Inactif"

#: modules/Lomiri/Components/Extras/Printers/utils.h:61
msgid "Long Edge (Standard)"
msgstr "Bord long (standard)"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:115
msgid "No messages"
msgstr "Pas de messages"

#: modules/Lomiri/Components/Extras/Printers/backend/backend_pdf.cpp:47
msgid "Normal"
msgstr "Normal"

#: modules/Lomiri/Components/Extras/Printers/utils.h:64
msgid "One Sided"
msgstr "Recto"

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:121
msgid "Redo"
msgstr "Rétablir"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:221
msgid "Revert Photo"
msgstr "Restaurer la photo"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:205
#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:128
msgid "Revert to original"
msgstr "Restaurer l'original"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:52
msgid "Rotate"
msgstr "Faire pivoter"

#: modules/Lomiri/Components/Extras/Printers/utils.h:59
msgid "Short Edge (Flip)"
msgstr "Bord court (pivoté)"

#: modules/Lomiri/Components/Extras/Example/Printers.qml:110
msgid "Stopped"
msgstr "Arrêté"

#: modules/Lomiri/Components/Extras/Printers/printers/printers.cpp:395
msgid "Test page"
msgstr "Page de test"

#: modules/Lomiri/Components/Extras/PhotoEditor.qml:206
msgid "This will undo all edits, including those from previous sessions."
msgstr ""
"Ceci annulera toutes les modifications, y compris celles des sessions "
"précédentes."

#: modules/Lomiri/Components/Extras/PhotoEditor/EditStack.qml:114
msgid "Undo"
msgstr "Annuler"

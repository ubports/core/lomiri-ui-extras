include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
)

set(QT_IMPORTS_DIR "${CMAKE_INSTALL_LIBDIR}/qt${QT_VERSION_MAJOR}/qml")
set(BARE_PLUGIN_DIR Lomiri/Components/Extras/PamAuthentication)
set(PLUGIN_DIR ${QT_IMPORTS_DIR}/${BARE_PLUGIN_DIR})

set(pamauthentication_SRCS
    pamauthentication.cpp
    pamauthentication.h
    pamauthentication_plugin.cpp
    pamauthentication_plugin.h
)

add_library(PamAuthentication MODULE
    ${pamauthentication_SRCS}
)

target_link_libraries(PamAuthentication
    pam
    Qt::Gui
    Qt::Qml
    Qt::Quick
    Qt::Widgets
    Qt::DBus
)

# Copy the plugin, the qmldir file and other assets to the build dir for running in QtCreator
if(NOT "${CMAKE_CURRENT_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_BINARY_DIR}")
    add_custom_command(TARGET PamAuthentication POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/../${BARE_PLUGIN_DIR}
        COMMENT "Creating plugin directory layout in the build directory"
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/qmldir ${CMAKE_CURRENT_BINARY_DIR}/../${BARE_PLUGIN_DIR}
        COMMENT "Copying the qmldir file to the build directory"
        COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:PamAuthentication> ${CMAKE_CURRENT_BINARY_DIR}/../${BARE_PLUGIN_DIR}
        COMMENT "Copying the plugin binary to the build directory"
    )
endif(NOT "${CMAKE_CURRENT_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_BINARY_DIR}")

# Install plugin file
install(TARGETS PamAuthentication DESTINATION ${PLUGIN_DIR})
install(FILES qmldir DESTINATION ${PLUGIN_DIR})
